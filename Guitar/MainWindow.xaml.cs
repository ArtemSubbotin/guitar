﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Guitar
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public ContentControl[,] _fretContainers = null;
    private List<NoteViewModel> _fretViewModels;

    public MainWindow()
    {
      InitializeComponent();
    }

    private void Draw()
    {
      fretPatternsContainer.Visibility = Visibility.Collapsed;

      var fretPatterns = fretPatternsContainer.Children.OfType<FrameworkElement>().Select((x, i) => new { Width = x.ActualWidth, Index = i });
      _fretContainers = new ContentControl[6, fretPatterns.Count()];


      var stringContainers = new List<StackPanel>();
      foreach (var stringGrid in new List<Grid>() { string1, string2, string3, string4, string5, string6 })
      {
        stringGrid.Children.Clear();
        var stringStac = new StackPanel() { Orientation = Orientation.Horizontal };
        stringGrid.Children.Add(stringStac);
        stringContainers.Add(stringStac);
      }


      var strings = GuitarString.GenClassicGuitarStrings(fretPatterns.Count()).ToList();

      _fretViewModels = new List<NoteViewModel>();

      var fretIndex = 0;
      foreach (var fretPattern in fretPatterns)
      {
        var stringIndex = 0;
        
        foreach (var stringContainer in stringContainers)
        {
          var fretViewModel = new NoteViewModel()
          {
            Note = strings[stringIndex].Notes[fretIndex + 1],
            StringIndex = stringIndex,
            IsSelected = false
          };

          var fretGrid = new ContentControl()
          {
            DataContext = fretViewModel,
            Template = this.Resources["fretLabelTemplate"] as ControlTemplate
          };

          _fretViewModels.Add(fretViewModel);

          fretGrid.MouseDown += FretGrid_MouseDown;

          var wrapGrid = new Grid() { HorizontalAlignment = HorizontalAlignment.Stretch, VerticalAlignment = VerticalAlignment.Stretch, Background = Brushes.Transparent, Width = fretPattern.Width };
          wrapGrid.Children.Add(fretGrid);

          stringContainer.Children.Add(wrapGrid);

          _fretContainers[stringIndex, fretIndex] = fretGrid;

          stringIndex++;
        }

        fretIndex++;
      }
      
    }

    private void FretGrid_MouseDown(object sender, MouseButtonEventArgs e)
    {
      var clickedNote = (sender as FrameworkElement).DataContext as NoteViewModel;

      var notes = Note.GenGammaNotes(clickedNote.Note.Name);
      var majorMap = new List<Boolean>() { true, false, true, false, true, true, false, true, false, true, false, true };
      var majorNoteNames = notes.Where((x, i) => majorMap[i]).Select(x => x.Name).ToList();

      foreach (var fretVM in _fretViewModels)
      {
        fretVM.IsSelected = majorNoteNames.Contains(fretVM.Note.Name);
        fretVM.IsTonic = fretVM.Note.Name == clickedNote.Note.Name;
      }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      Draw();
    }

    private void btnMagor_Click(object sender, RoutedEventArgs e)
    {

    }
  }

  public class GuitarString
  {
    private static List<String> ClassicTune = new List<String> { "E", "B", "G", "D", "A", "E" };

    public Int32 Index { get; set; }
    public List<Note> Notes { get; set; }

    public GuitarString(Int32 stringIndex, Int32 fretsCount)
    {
      Index = stringIndex;
      Notes = Note.GenNotes(ClassicTune[stringIndex], fretsCount + 1); //+1 - open string
    }

    public static IEnumerable<GuitarString> GenClassicGuitarStrings(Int32 fretsCount)
    {
      for (int i = 0; i < 6; i++)
      {
        yield return new GuitarString(i, fretsCount);
      }
    }
  }

  public class Note
  {
    public String Name { get; private set; }
    public Boolean IsSharp { get; private set; }
    
    public Note(String name)
    {
      Name = name.ToUpper();
      IsSharp = name.Contains("#");
    }

    public static List<Note> GenNotes(String startNoteName, Int32 semitoneCount)
    {
      var res = new List<Note>(semitoneCount);
      res.AddRange(GenGammaNotes().SkipWhile(x => x.Name != startNoteName));
      while (res.Count < semitoneCount)
      {
        res.AddRange(GenGammaNotes());
      }
      return res.Take(semitoneCount).ToList();
    }

    public static List<Note> GenGammaNotes()
    {
      return "C C# D D# E F F# G G# A A# B".Split(' ').Select(x => new Note(x)).ToList();
    }

    public static List<Note> GenGammaNotes(String startNoteName)
    {
      return GenNotes(startNoteName, 12);
    }
  }

  public class NoteViewModel : INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;
    public void Notify(String name)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(name));
    }

    private Note _note;
    public Note Note
    {
      get { return _note; }
      set { _note = value; Notify(nameof(Note)); }
    }

    private Boolean _isSelected;
    public Boolean IsSelected
    {
      get { return _isSelected; }
      set { _isSelected = value; Notify(nameof(IsSelected)); }
    }

    private Int32 _stringIndex;
    public Int32 StringIndex
    {
      get { return _stringIndex; }
      set { _stringIndex = value; Notify(nameof(StringIndex)); }
    }

    private Boolean _isTonic;
    public Boolean IsTonic
    {
      get { return _isTonic; }
      set { _isTonic = value; Notify(nameof(IsTonic)); }
    }
  }
}
